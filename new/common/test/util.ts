import { isNotBlank, isNotNull } from "#self/util";
describe("isNotNull", () => {
    it("should return true for non-null values", () => {
        for (const value of [0, "0", "", "hello!", NaN, "1", "    "]) {
            expect(isNotNull(value)).toBeTruthy();
        }
    });
    it("should return false for null values", () => {
        for (const value of [null, undefined]) {
            expect(isNotNull(value)).toBeFalsy();
        }
    });
});

describe("isNotBlank", () => {
    it("should return true for non-blank strings", () => {
        for (const value of [
            "abababa",
            "this is not empty",
            "...",
            ".",
            "0",
            "1",
        ]) {
            expect(isNotBlank(value)).toBeTruthy();
        }
    });
    it("should return false for blank strings", () => {
        for (const value of ["    ", " ", "\n", "\n\n", "\r", "\t"]) {
            expect(isNotBlank(value)).toBeFalsy();
        }
    });
});
