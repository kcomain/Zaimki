// Taken from old codebase
const MIN_USERNAME_LENGTH = 4;
const MAX_USERNAME_LENGTH = 17; // No usernames are longer than 17 characters at the moment
const VALID_USERNAME_REGEX = /^[\p{L}\p{N}._-]+$/u;
const EMAIL_LOCAL_PART_ALLOWED = /^[A-Za-z0-9.\-_]$/u;

export function isValidUsername(str: string): boolean {
    if (str.length < MIN_USERNAME_LENGTH || str.length > MAX_USERNAME_LENGTH) {
        return false;
    }
    return VALID_USERNAME_REGEX.test(str);
}


/**
 * Validates an email address.
 *
 * Note that this does not validate e-mail addresses according to what is allowed by SMTP;
 * instead it uses a much more restrictive set of rules as to avoid conflict with other e-mail providers.
 */
export function isValidEmail(str: string): boolean {
    // See https://www.rfc-editor.org/errata/eid1003
    if (str.length >= 256) {
        return false;
    }
    const parts = str.split("@");
    if (parts.length !== 2) {
        return false;
    }
    const [localPart, domain] = parts;

    // These limits are mainly to prevent spam
    if (localPart.length > 64 || domain.length > 255) {
        return false;
    }

    let beginningOfPart = true;
    for (const c of localPart) {
        switch (c) {
            case '+':
            case '.':
                if (beginningOfPart) {
                    return false; // Consecutive dots are not allowed
                }
                beginningOfPart = true;
                continue;
            default: {
                if (!EMAIL_LOCAL_PART_ALLOWED.test(c)) {
                    return false;
                }
                beginningOfPart = false;
                continue;
            }
        }
    }
    if (beginningOfPart) {
        // The local part ended with a special character, which is not allowed in this manner.
        return false;
    }
    return true;
}