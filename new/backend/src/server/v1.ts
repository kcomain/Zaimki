import { Type } from "@sinclair/typebox";
import {
    allPronounVariants,
    examplesFor,
    Locale,
    Pronoun,
    PronounExample,
} from "#self/locales";
import pronouns from "#self/server/v1/pronouns";
import inclusive from "#self/server/v1/inclusive";

export type V1AppInstance = AppInstance;

export interface ApiErrorOptions {
    code: number;
    message: string;
    detail?: string;
}

/**
 * Creates an error object for the v1 API endpoints.
 * This function may seem a bit extraneous but it's a useful utility for consistency.
 * @param options
 */
export function error<T extends ApiErrorOptions>(options: T) {
    const response: Record<string, unknown> = {
        code: options.code,
        message: options.message,
    };
    if (options.detail) {
        response.detail = options.detail;
    }
    return response;
}

export function replyError<T extends ApiErrorOptions>(
    reply: AppReply,
    options: T
): AppReply {
    const response = error(options);
    return reply.status(options.code).send(response);
}

export const ApiError = {
    BAD_REQUEST: {
        code: 400,
        message: "Bad request",
        detail: "The request you've sent is malformed or otherwise invalid according to our code.",
    },
    INVALID_LOCALE: {
        code: 404,
        message: "Invalid locale",
    },
    UNKNOWN_PRONOUN: {
        code: 404,
        message:
            "We aren't aware of any such pronoun. Perhaps there's a typo, or perhaps you specified an incorrect number of morphemes?",
        detail: "Note that for custom pronouns, you need to specify all necessary forms: https://en.pronouns.page/faq#custom-pronouns",
    },
} satisfies Record<string, ApiErrorOptions>;

export const localeSpecific = Type.Object({
    locale: Type.String(),
});

export interface TransformPronounOptions {
    processName: boolean;
    isUserGenerated: boolean;
}
export function transformPronoun(
    pronoun: Pronoun,
    examples: Array<PronounExample>,
    locale: Locale,
    options?: Partial<TransformPronounOptions>
) {
    const opts: TransformPronounOptions = {
        processName: options?.processName ?? false,
        isUserGenerated: options?.isUserGenerated ?? false,
    };
    const morphemes: Record<string, string> = {};
    const pronunciations: Record<string, string> = {};
    for (const [name, form] of Object.entries(pronoun.forms)) {
        morphemes[name] = allPronounVariants(form)
            .map((v) => v.written)
            .join("&");
        pronunciations[name] = form.pronounced;
    }
    return {
        canonicalName: pronoun.canonicalName,
        description: pronoun.description ?? "",
        normative: pronoun.isNormative,
        morphemes,
        pronunciations,
        plural: [pronoun.isPlural],
        pluralHonorific: [pronoun.isPluralHonorific],
        aliases: pronoun.keys.slice(1),
        history: opts.isUserGenerated ? "__generator__" : pronoun.history ?? "",
        pronounceable: pronoun.isPronounceable,
        thirdForm: pronoun.thirdForm ?? null,
        smallForm: pronoun.smallForm ?? null,
        sourcesInfo: pronoun.source ?? null,
        examples: examplesFor(pronoun, examples),
        name: opts.processName ? locale.pronounName(pronoun) : undefined,
    };
}

/*
 * Version 1 of the API. This would be the "legacy" version.
 * Since the output of every endpoint is locale-specific,
 * every route is prefixed with the respective locale ID.
 */

export const routes = async function (app: AppInstance) {
    app.register(pronouns);
    app.register(inclusive);
} satisfies AppPluginAsync;
export default routes;
