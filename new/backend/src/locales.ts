/*
 * This file is for interop with the old locales.js file to reduce duplication.
 */

import { getConfig } from "#self/config";
import * as path from "node:path";
import * as suml from "@pronounspage/common/suml";
import fsp from "node:fs/promises";
import log from "#self/log";
import * as csv from "csv-parse/sync";
import {
    capitalizeFirstLetter,
    compileTemplate,
    isNotBlank,
    parseBool,
} from "@pronounspage/common/util";

export interface LocaleDescription {
    code: string;
    name: string;
    url: string;
    published: boolean;
    /**
     * This locale's language family.
     *
     * @experimental
     * This is currently not used for published versions.
     */
    symbol: string;
    /**
     * This locale's language family.
     *
     * @experimental
     * This is currently not used for published versions.
     *
     * @privateRemarks
     * This could technically be an enum but for now it's just a string as there
     * doesn't seem to be any apparent benefit to including locales
     */
    family: string;
}

let expectedTranslations: Array<string>;

let loadedDescriptions: Array<LocaleDescription>;
let indexedDescriptions: Record<string, LocaleDescription>;
let activeLocaleDescriptions: Array<LocaleDescription> | undefined;

export async function loadLocaleDescriptions(): Promise<
    Array<LocaleDescription>
> {
    if (loadedDescriptions == undefined) {
        loadedDescriptions = (
            await import(
                "file://" +
                    path.resolve(getConfig().localeDataPath, "locales.js")
            )
        ).default;
        indexedDescriptions = Object.fromEntries(
            loadedDescriptions.map((v) => [v.code, v])
        );
        expectedTranslations = (
            await import(
                "file://" +
                    path.resolve(
                        getConfig().localeDataPath,
                        "expectedTranslations.js"
                    )
            )
        ).default;
        activeLocaleDescriptions = undefined;
    }
    return loadedDescriptions;
}

export function getCachedLocaleDescriptions(): Array<LocaleDescription> {
    return loadedDescriptions;
}

export function getActiveLocaleDescriptions(): Array<LocaleDescription> {
    if (activeLocaleDescriptions == undefined) {
        const config = getConfig();
        const cached = getCachedLocaleDescriptions();

        activeLocaleDescriptions = [];
        for (const locale of cached) {
            if (config.allowUnpublishedLocales || locale.published) {
                activeLocaleDescriptions.push(locale);
            }
        }
    }
    return activeLocaleDescriptions;
}

export function getExpectedTranslations(): Array<string> {
    if (!Array.isArray(expectedTranslations)) {
        throw new Error("Expected translations have not been loaded yet");
    }
    return expectedTranslations;
}

export function isLocaleActive(localeCode: string): boolean {
    return getActiveLocaleDescriptions().some((v) => v.code === localeCode);
}

export interface PronounVariant {
    written: string;
    pronounced: string;
}
export interface PronounForm extends PronounVariant {
    variants?: Array<PronounVariant>;
}
export interface Pronoun {
    keys: Array<string>;
    description?: string;
    history?: string;
    source?: string;
    isNormative: boolean;
    isPlural: boolean;
    isPluralHonorific: boolean;
    isPronounceable: boolean;

    thirdForm?: string;
    smallForm?: string;

    forms: Record<string, PronounForm>;

    canonicalName: string;
}

export function allPronounVariants(form: PronounForm): Array<PronounVariant> {
    const arr: Array<PronounVariant> = [
        {
            written: form.written,
            pronounced: form.pronounced,
        },
    ];
    if (form.variants) {
        form.variants.map((v) => arr.push(v));
    }
    return arr;
}

export function parsePronounFromParts(
    parts: Array<string>,
    forms: Array<string>
): Pronoun | null {
    if (parts.length !== forms.length) {
        return null;
    }

    const pronounForms: Pronoun["forms"] = {};
    for (let i = 0; i < forms.length; i++) {
        const [written, pronounced] = parts[i].split("|");

        const writtenVariants = written.split("&");
        const pronouncedVariants = pronounced?.split("&") ?? [];

        const current: PronounForm = {
            written: writtenVariants.shift()!,
            pronounced: pronouncedVariants.shift()!,
        };

        if (writtenVariants.length || pronouncedVariants.length) {
            const length = Math.max(
                writtenVariants.length,
                pronouncedVariants.length
            );
            const lastWritten = writtenVariants.length - 1,
                lastPronounced = pronouncedVariants.length - 1;
            const variants: PronounForm["variants"] = [];
            for (let i = 0; i < length; i++) {
                variants.push({
                    written:
                        writtenVariants[Math.max(i, lastWritten)] ??
                        current.written,
                    pronounced:
                        pronouncedVariants[Math.max(i, lastPronounced)] ??
                        current.pronounced,
                });
            }
            current.variants = variants;
        }
        pronounForms[forms[i]] = current;
    }

    return {
        forms: pronounForms,
        isNormative: false,
        keys: [],
        isPronounceable: false,
        isPlural: false,
        isPluralHonorific: false,
        canonicalName: `${parts[0]}/${parts[1]}`,
    };
}
export function parsePronounFromString(s: string, forms: Array<string>) {
    return parsePronounFromParts(s.split("/"), forms);
}

export interface PronounExample {
    singular?: string;
    plural?: string;
    isHonorific: boolean;
}

export class Locale {
    private _code: string;
    private _dataDirectory: string;

    constructor(code: string, dataDirectory: string) {
        this._code = code;
        this._dataDirectory = dataDirectory;
    }

    private path(pathParts: Array<string>) {
        return path.resolve(this._dataDirectory, ...pathParts);
    }

    private async readFile(parts: Array<string>): Promise<string>;
    private async readFile<T>(
        parts: Array<string>,
        parser: (value: string) => T
    ): Promise<T>;
    private async readFile<T>(
        pathParts: Array<string>,
        parser?: (value: string) => T
    ) {
        // Some may complain about this
        const filePath = this.path(pathParts);
        try {
            log.trace(`[${this.code}] Loading file ${filePath}`);
            const data = await fsp.readFile(filePath, "utf-8");
            if (parser) {
                return parser(data);
            } else {
                return data;
            }
        } catch (e) {
            const err = new Error(
                `[${this.code}] Could not load file ${filePath}: ${e}`
            );
            err.cause = e;
            throw err;
        }
    }

    private async importFile(parts: Array<string>): Promise<unknown> {
        const filePath = this.path(parts);
        try {
            log.trace(`[${this.code}] Importing file ${filePath} for locale`);
            return await import(`file://${filePath}`);
        } catch (e) {
            const err = new Error(
                `[${this.code}] Could not import file ${filePath}: ${e}`
            );
            err.cause = e;
            throw err;
        }
    }

    private _config: any | null = null;
    private _translations: unknown | null = null;
    private _pronouns: Array<Pronoun> = [];
    private _pronounsByAlias: Record<string, number> = {};

    private _examples: Array<PronounExample> = []; // TODO: Type these properly
    private _morphemes: Array<string> = [];

    public async load() {
        const tsvParse = (data: string) =>
            csv.parse(data, {
                columns: true,
                cast: false,
                relaxColumnCount: true,
                relaxQuotes: true,
                delimiter: "\t",
            });
        this._config = await this.readFile(["config.suml"], suml.parse);
        this._translations = await this.readFile(
            ["translations.suml"],
            suml.parse
        );

        // NOTE(tecc): This is a hack. A bad hack.
        //             But it's far less painful than converting every morphemes.js
        //             file to some other format.
        this._morphemes = await this.readFile(
            ["pronouns/morphemes.js"],
            (value) => {
                let all = [];
                let capture = null;
                for (const char of value) {
                    if (capture == null) {
                        if (char === '"' || char === "'") {
                            capture = "";
                        }
                    } else {
                        if (char === '"' || char === "'") {
                            all.push(capture);
                            capture = null;
                            continue;
                        }
                        capture += char;
                    }
                }
                return all;
            }
        );
        this._pronouns = [];
        this._pronounsByAlias = {};
        const pronouns = await this.readFile(
            ["pronouns/pronouns.tsv"],
            tsvParse
        );
        for (const pronoun of pronouns) {
            const partial: Partial<Pronoun> = { forms: {} };
            for (const [key, value] of Object.entries<string>(pronoun)) {
                switch (key) {
                    case "key":
                        partial.keys = value.split(",");
                        break;
                    case "normative":
                        partial.isNormative = parseBool(value);
                        break;
                    case "plural":
                        partial.isPlural = parseBool(value);
                        break;
                    case "pluralHonorific":
                        partial.isPluralHonorific = parseBool(value);
                        break;
                    case "pronounceable":
                        partial.isPronounceable = parseBool(value);
                        break;
                    case "description":
                    case "history":
                    case "thirdForm":
                    case "smallForm":
                        partial[key] = isNotBlank(value) ? value : undefined;
                        break;
                    case "sourcesInfo":
                        partial.source = value;
                        break;
                    default:
                        if (!this._morphemes.includes(key)) {
                            throw new Error(
                                `[${this.code}] Unknown key ${key}`
                            );
                        }
                        const [written, pronounced] = value.split("|");
                        partial.forms![key] = { written, pronounced };
                        break;
                }
            }
            const i = this._pronouns.length;
            const obj = partial as Pronoun;
            this._pronouns.push(obj);
            for (const key of obj.keys) {
                this._pronounsByAlias[key] = i;
            }
            obj.canonicalName = obj.keys[0];
        }
        const examples = await this.readFile(
            ["pronouns/examples.tsv"],
            tsvParse
        );
        this._examples = [];
        for (const example of examples) {
            this._examples.push({
                singular: isNotBlank(example.singular)
                    ? example.singular
                    : undefined,
                plural: isNotBlank(example.plural) ? example.plural : undefined,
                isHonorific: example.isHonorific,
            } satisfies PronounExample);
        }
    }

    public get code() {
        return this._code;
    }

    public get description() {
        return indexedDescriptions[this.code];
    }

    public get config() {
        return this._config;
    }

    public get pronouns() {
        return Object.values(this._pronouns);
    }

    public pronoun(key: string): Pronoun | null {
        const index = this._pronounsByAlias[key];
        if (index == null) {
            return null;
        }
        return this._pronouns[index];
    }

    public pronounNameVariants(pronoun: Pronoun): Array<string> {
        // NOTE(tecc): This is an attempt at decoding the code from classes.js.
        //             I make no guarantee regarding the accuracy.
        const variants: Set<string> = new Set();

        function allVariants(form: PronounForm): Array<string> {
            return [
                form.written,
                ...(form.variants?.map((v) => v.written) ?? []),
            ];
        }

        const firstMorpheme = allVariants(pronoun.forms[this.morphemes[0]]);
        if (this.morphemes.length === 1) {
            return firstMorpheme;
        }

        const secondMorpheme = allVariants(pronoun.forms[this.morphemes[1]]);
        const thirdMorpheme =
            this.morphemes.length > 2
                ? allVariants(pronoun.forms[this.morphemes[2]])
                : [];

        let thirdFormMorpheme = pronoun.thirdForm
            ? allVariants(pronoun.forms[pronoun.thirdForm])
            : [];

        let thirdMorphemeThreeForms = thirdMorpheme;
        if (this.code === "ru" || this.code === "ua") {
            thirdMorphemeThreeForms = thirdMorpheme.map((x) => `[-${x}]`);
        }

        for (let i = 0; i < firstMorpheme.length; i++) {
            let firstPart = firstMorpheme[i];
            let secondPart =
                secondMorpheme[Math.min(i, secondMorpheme.length - 1)];
            if (
                firstPart === secondPart &&
                thirdMorpheme.length &&
                !this.config.pronouns.threeForms
            ) {
                secondPart =
                    thirdMorpheme[Math.min(i, thirdMorpheme.length - 1)];
            }
            let variant = firstPart + "/" + secondPart;
            if (this.config.pronouns.threeForms) {
                variant +=
                    "/" +
                    thirdMorphemeThreeForms[
                        Math.min(i, thirdMorphemeThreeForms.length - 1)
                    ];
            } else if (pronoun.thirdForm) {
                variant +=
                    "/" +
                    thirdFormMorpheme[
                        Math.min(i, thirdFormMorpheme.length - 1)
                    ];
            }

            variants.add(variant);
        }
        return [...variants];
    }
    public pronounName(pronoun: Pronoun, separator: string = ","): string {
        return this.pronounNameVariants(pronoun).join(separator);
    }

    public get examples() {
        return this._examples;
    }
    public get morphemes() {
        return this._morphemes;
    }
}

export function examplesFor(
    pronoun: Pronoun,
    examples: Array<PronounExample>
): Array<string> {
    const finished = [];

    const templating: Record<string, string> = {};
    for (const [key, value] of Object.entries(pronoun.forms)) {
        templating[key] = value.written;
        templating[`'${key}`] = capitalizeFirstLetter(value.written);
    }

    for (const example of examples) {
        let template = example.singular;
        if (pronoun.isPlural && example.plural) {
            template = example.plural;
        }
        if (template == null) {
            continue;
        }
        finished.push(compileTemplate(template, templating));
    }
    return finished;
}

const loadedLocales: Record<string, Locale> = {};

/**
 * Load a locale.
 * This will only succeed if the locale is both
 * @param localeCode the locale code
 */
export function getLocale(localeCode: string): Locale | null {
    if (!isLocaleActive(localeCode)) {
        return null;
    }
    let locale = loadedLocales[localeCode];
    if (locale == null) {
        locale = new Locale(
            localeCode,
            path.resolve(getConfig().localeDataPath, localeCode)
        );
        loadedLocales[localeCode] = locale;
    }
    return locale;
}

export async function loadAllLocales() {
    const descriptions = await loadLocaleDescriptions();
    const promises = [];
    for (const description of descriptions) {
        const locale = getLocale(description.code);
        if (locale) {
            promises.push(
                locale
                    .load()
                    .then(() =>
                        log.debug(`Successfully loaded locale ${locale.code}`)
                    )
            );
        }
    }
    await Promise.all(promises);
    log.info("All active locales have been loaded into memory");
}
