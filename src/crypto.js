import crypto from 'crypto';
import fs from 'fs'

class Crypto {
    constructor(privateKey, publicKey) {
        this.privateKey = crypto.createPrivateKey(fs.readFileSync(privateKey));
        this.publicKey = crypto.createPublicKey(fs.readFileSync(publicKey));
    }

    sign(payload) {
        const sign = crypto.createSign('SHA256');
        sign.update(payload);
        return sign.sign(this.privateKey, 'hex');
    }

    validate(payload, signature) {
        const verify = crypto.createVerify('SHA256');
        verify.update(payload);
        return verify.verify(this.publicKey, signature, 'hex');
    }
}

export default new Crypto(__dirname + '/../keys/private.pem', __dirname + '/../keys/public.pem');
