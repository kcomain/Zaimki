const allLocales = require('../locale/locales')

module.exports = (current, includeUnpublished = false) => {
    const d = {};
    for (let locale of allLocales) {
        if (locale.published || current === locale.code || includeUnpublished) {
            d[locale.code] = locale;
        }
    }

    // hoist current to top, then published, then unpublished
    const sortedLocales = {};
    if (d[current] !== undefined) {
        sortedLocales[current] = d[current];
    }
    for (let [code, localeConfig] of Object.entries(d)) {
        if (code !== current && localeConfig.published) {
            sortedLocales[code] = localeConfig;
        }
    }
    for (let [code, localeConfig] of Object.entries(d)) {
        if (code !== current && !localeConfig.published) {
            sortedLocales[code] = localeConfig;
        }
    }

    return sortedLocales;
}
