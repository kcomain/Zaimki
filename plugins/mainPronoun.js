import { pronouns } from "../src/data";
import { buildPronoun } from "../src/buildPronoun";
import opinions from "../src/opinions";

export default {
    computed: {
        pronounOpinions() {
            const pronounOpinions = [];
            if (!this.profile) {
                return pronounOpinions;
            }
            for (let {value: pronoun, opinion} of this.profile.pronouns) {
                let link = pronoun
                    .trim()
                    .replace(new RegExp('^' + this.$base), '')
                    .replace(new RegExp('^' + this.$base.replace(/^https?:\/\//, '')), '')
                    .replace(new RegExp('^/'), '');

                try {
                    link = decodeURIComponent(link);
                } catch {
                    continue;
                }

                if (!link.startsWith(':') && this.config.locale !== 'tok') {
                    link = link.toLowerCase();
                }

                const linkNorm = link.toLowerCase();
                if (linkNorm === this.config.pronouns.any
                    || linkNorm.startsWith(this.config.pronouns.any + ':')
                    || (this.config.pronouns.null && this.config.pronouns.null.routes && this.config.pronouns.null.routes.includes(linkNorm))
                    || (this.config.pronouns.mirror && this.config.pronouns.mirror.route === linkNorm)
                ) {
                    pronounOpinions.push({
                        link,
                        pronoun: link.replace(/:+/g, ' '),
                        opinion,
                    });
                    continue;
                }

                const pronounEntity = buildPronoun(pronouns, link);

                if (pronounEntity) {
                    pronounOpinions.push({
                        link,
                        pronoun: pronounEntity,
                        opinion,
                    });
                }
            }
            return pronounOpinions;
        },
        mainPronoun() {
            if (!this.config.profile?.flags?.defaultPronoun) {
                return null;
            }
            let mainPronoun = buildPronoun(pronouns, this.config.profile.flags.defaultPronoun);
            let mainOpinion = -1;
            for (let {pronoun, opinion} of this.pronounOpinions) {
                if (typeof pronoun === 'string') {
                    continue;
                }
                const opinionValue = opinions[opinion]?.value || 0;
                if (opinionValue > mainOpinion) {
                    mainPronoun = pronoun;
                    mainOpinion = opinionValue;
                }
            }

            return mainPronoun;
        },
    }
}
