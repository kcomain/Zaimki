-- Up

UPDATE inclusive SET categories = replace(categories, 'disability', 'disability and neurodiversity') WHERE locale = 'en';
UPDATE inclusive SET categories = replace(categories, 'niepełnosprawność', 'niepełnosprawność i neuroróżnorodność') WHERE locale = 'pl';

-- Down
