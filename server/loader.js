const fs = require('fs');
import Suml from 'suml';
import Papa from 'papaparse';

export const loadSumlFromBase = name => new Suml().parse(fs.readFileSync(`./${name}.suml`, 'utf-8'))
export const loadSuml = name => loadSumlFromBase(`data/${name}`);

export const loadTsv = name => Papa.parse(fs.readFileSync(`./data/${name}.tsv`).toString(), {
    dynamicTyping: true,
    header: true,
    skipEmptyLines: true,
    delimiter: '\t',
}).data;
