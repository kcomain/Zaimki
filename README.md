# Zaimki.pl / Pronouns.page

## Installation

```
git clone git@gitlab.com:PronounsPage/PronounsPage.git
```

We're using FontAwesome Pro, so to set up a local copy without having a FA license,
open `package.json` and replace

```
"@fortawesome/fontawesome-pro": "git+ssh://git@gitlab.com:Avris/FontAwesomePro.git",
```

with

```
"@fortawesome/fontawesome-pro": "git+ssh://git@gitlab.com:Avris/FakeFontAwesomePro.git",
```
or, for Git via HTTPS:

```
"@fortawesome/fontawesome-pro": "git+https://git@gitlab.com/Avris/FakeFontAwesomePro.git",
```

Do not commit that change!

## Build Setup

```bash
# install dependencies
$ make install

# configure environment
$ nano .env
$ make switch LANG=pl

# serve with hot reload at localhost:3000
$ make run

# build for production and launch server
$ make deploy
$ nuxt start
```

## Copyright

See [LICENSE](./LICENSE.md).

## Development Documentation
Read [this](https://en.pronouns.page/new-version) first. \
When you add a new language, remember to modify the file `./locale/locales.js` to add your language:
```js

// Add an object according to the following format:
{code: 'xx', name: 'xx', url: 'https://xx.pronouns.page', published: false , symbol: 'x', family: 'xxx'}

// Setting 'published' to true means that the language was published onto the main site.
// For example: 
{code: 'pt', name: 'Português', url: 'https://pt.pronouns.page', published: true, symbol: 'ã', family: 'romance'}

```
If you're having problems using Yarn, npm is probably fine, but remember to switch any `yarn [x]` commands for `npm run [x]`.\
Remember to modify the `.env` file. You don't really need to set up any external APIs, just make up a secret.
```text
// ...
SECRET=replaceThis
// ...
```
In order to test the login system, you can type in an email followed by a + (example@pronouns.page+). This way, you won't need a mailer, as the code will always be 999999. This feature is exclusive to development builds.

### Windows

Current setup depends on working symlinks, which means that an NTFS filesystem is unfortunately required for Windows at the moment.

If you encounter a `ParseError`, make sure the file is in `\n` (_Line Feed_) mode instead of `\r\n` (_Carriage Return Line Feed_).

If you want to automate the change, [try this](https://stackoverflow.com/a/50006730). \
Make sure that the `.editorconfig` looks like this:
```text
[*.suml]
end_of_line = lf
```
### Password

For unpublished locales and for the test server, a password is required to access the web interface.
If you're on our Discord, you can ask for it there (or just use `published: true` for local development).

### Troubleshooting

If you're having issues with [HRM](https://webpack.js.org/concepts/hot-module-replacement/) (Hot Module Replacement) not reloading automatically: 
- The modules within `package.json` are matching the repo's `package.json`.
- `nuxt.config.js` has the following option:
```js
watchers: {
    webpack: {
        aggregateTimeout: 300,
        poll: 1000
    }
}
```

## Current translations being worked on
- Catalan - @peiprjs 
- Hebrew - @ThatTransGirl
- [Add yours!]
