// sort Nominativ first and Dativ second to keep the convention that the first two morphemes identify the pronoun set
const cases = ["n", "d", "a", "g"];

const morphemeByCase = morphemeType => {
    return cases.map(caseAbbreviation => `${morphemeType}_${caseAbbreviation}`);
};

const morphemeGroups = [
    morphemeByCase('pronoun'),
    morphemeByCase('possessive_f'),
    morphemeByCase('possessive_m'),
    morphemeByCase('possessive_n'),
    morphemeByCase('article'),
    morphemeByCase('demonstrative'),
];
export default morphemeGroups.flatMap(morphemeGroup => morphemeGroup);
